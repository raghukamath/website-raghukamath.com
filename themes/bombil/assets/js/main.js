/*
    License : MIT
    Author: Raghavendra Kamath
    Email: raghu@raghukamath.com
    Copyright 2019 Raghavendra Kamath
    This script is for the infamous hamburger menu on this website and scroll to top functionality

    
    @licstart  The following is the entire license notice for the 
        JavaScript code in this page.

       Copyright 2019 Raghavendra Kamath

        The JavaScript code in this page is free software and is licensed under MIT license
        
        Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

        The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

        THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

    @licend The above is the entire license notice for the JavaScript code in this page.
            

/* dropdown menu for mobile */

const hamburger = document.getElementById("js-button");
const navClass = document.getElementById("js-dropdown");

function replaceClass() {
	navClass.classList.toggle('hide');
}

hamburger.addEventListener ('click' , replaceClass);

/* Scroll to top */

const scrollTop = document.getElementById("js-top");
scrollTop.addEventListener ('click', event => {
	window.scrollTo(0, 0);
});
