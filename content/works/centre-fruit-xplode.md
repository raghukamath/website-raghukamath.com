+++
title = "Centre Fruit Explode"
date =  "2015-11-30T18:10:00+05:30"
workthumb = "/img/thumb1-300x300.jpg"
thumb = "/img/thumb1-600.jpg"
tags = ["realistic"]
client = "Ogilvy Mumbai"
description = "Illustration done for Center Fruit Xplode poster."
keywords = ["Center fruit xlplode", "illustration", "mumbai", "center fruit", "xplode", "ogilvy mumbai"]
+++

Illustration done for Center Fruit Xplode poster. This poster was done
with Ketan Mhabadi and Aditi Shah from Ogilvy Mumbai.

## Option One

![center fruit poster illustration](/img/Xplode-01.jpg)

## Option Two
This option is what the client approved (Typo and Layout
arrangements done by Ketan & Aditi)

![center fruit poster illustration](/img/CF_Xplode_poster_final-06.jpg)
