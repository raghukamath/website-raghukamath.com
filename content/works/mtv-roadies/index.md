+++
title = "Mtv Roadies Revolution"
date = 2020-03-31T11:00:34+05:30
client = "Viacom18"
thumb = "/works/mtv-roadies/mtv-roadies-thumb-600.jpg"
workthumb = "/works/mtv-roadies/mtv-roadies-thumb-300.jpg"
tags = ["realistic", "commission"]
keywords = ["Viacom", "Mtv India", "roadies", "outdoor hoarding", "roadies revolution", "ranvijay illustration", "mtv illustration", "Krita CMYK"]
description = "Illustrations done for MTV India's Roadies Revolution Launch campaign"
+++

My [studio](https://emblik.studio) collaborated with [Sameer Kulkarni](https://kulkarnisameer.com/) to create illustrations for the 18th season of MTV India's youth reality television show - Roadies Revolution. We were tasked to create portraits of the hosts of the show in a unique style. The portraits were then used to prepare various creatives required for the launch campaign. The artwork was adapted into outdoor hoarding (billboards) posters and online social media posts.

[Krita](https://krita.org) was used to create the unique grainy look in the portraits and blender was used to create some background elements.

![Mtv roadies illustration](mtv-roadies-001.jpg)

![Mtv roadies illustration](mtv-roadies-002.jpg)

Some background elements -

![Mtv roadies illustration](mtv-roadies-003.jpg)

Details in the portraits -

![Mtv roadies illustration - neha dhupia](mtv-roadies-004.jpg)

![Mtv roadies illustration - ranvijay](mtv-roadies-005.jpg)

[Blender](https://blender.org) used for creating the iconic [Lakshman_Jhula](https://en.wikipedia.org/wiki/Lakshman_Jhula).

![Mtv roadies illustration - ranvijay](blender-roadies.jpg)

Illustrations used in the final designs and adaptation by Viacom18 design team

![Mtv roadies illustration - design](mtv-roadies-revolution-design-1.jpg)

![Mtv roadies illustration - poster](mtv-roadies-revolution-design-2.jpg)

Photograph showing the illustration on the hoarding (billboard) in Mumbai.

![Mtv roadies illustration - billboard](MTV-roadies-billboard.jpg)

Artwork is under copyright of Viacom18.
