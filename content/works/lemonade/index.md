+++
title = "Lemonade"
date = 2019-04-29T14:00:34+05:30
thumb = "/works/lemonade/lemonade-thumb-600.jpg"
workthumb = "/works/lemonade/lemonade-thumb-300.jpg"
tags = ["personal", "cc-by"]
keywords = ["lemon", "summer", "krita", "lady", "indian", "lime green"]
description = "An Inking and coloring practice, I did last weekend. The theme was to create a refreshing illustration of a lady in summer"
+++

An Inking and coloring practice, I did last weekend. The theme was to create a refreshing illustration of a lady in summer

![digital painting dipicting a lady looking back](summer-lemon.jpg)

Artwork is under CC-BY-SA-4.0, source file can be downloaded from - [here](https://box.raghukamath.com/cloud/index.php/s/DnWTBBpj4iFnGB9).


