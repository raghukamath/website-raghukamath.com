+++
title = "Mtv Biryani Emoticon Posters"
date = 2019-11-24T11:00:34+05:30
client = "Viacom18"
thumb = "/works/mtv-emoticon/mtv-thumb-600.jpg"
workthumb = "/works/mtv-emoticon/mtv-thumb-300.jpg"
tags = ["graphic"]
keywords = ["Viacom", "Mtv India", "Biryani Emoticon", "Biryani", "Chicken Illustration", "goats", "emoticon"]
description = "Poster Illustrations for MTV India's Biryani Emoticon Campaign"
+++

My [studio](https://emblik.studio) was commissioned by Viacom to make posters for their campaign demanding emoticon for Biryani dish. These are some Illustrations that I did for them. These were done along the style of propaganda posters but I think in the end it drifted towards normal style a bit.

![Chicken Biryani Illustration](biryani-chicken.jpg)  

![Chicken Biryani Details](biryani-chicken-01.jpg)  

![Mutton Biryani Illustration](biryani-mutton.jpg)  

![Mutton Biryani Illustration details](biryani-mutton-01.jpg)  

Illustrations adapted in layout for the emailer created by the MTV India Design team.

![Chicken Biryani layout](mtv-chicken.jpg)  

![Mutton Biryani layout](mtv-mutton.jpg)  


Artwork is under copyright of Viacom18.
