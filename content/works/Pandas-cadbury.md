+++
title = "Pandas for Cadbury gems"
date = "2015-11-30T16:52:00+05:30"
workthumb = "/img/cthumb-300x300.jpg"
thumb = "/img/cthumb-600.jpg"
tags = ["realistic"]
client = "Cadbury India"
description = "Series Of Panda Illustrations done for Cadbury India with Ogilvy Mumbai"
keywords  = ["panda", "cadbury panda", "gems panda", "panda", "illustrations", "mumbai", "india"]
slug = "pandas-for-cadbury-gems"
+++

Series Of Panda Illustrations done for Cadbury India with Ogilvy Mumbai

![Cadbury gems boxer panda](/img/panda/c1.jpg)

![Cadbury gems karate panda](/img/panda/c2.jpg)

![Cadbury gems skater panda](/img/panda/c3.jpg)
