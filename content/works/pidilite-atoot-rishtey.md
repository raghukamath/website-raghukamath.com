+++
title = "Pidilite Fevicol - Atoot Rishtey"
date = "2015-11-30T18:51:00+05:30"
workthumb = "/img/pidilite-thumb-300x300.jpg"
thumb = "/img/pidilite-thumb-600.jpg"
tags = ["graphic"]
client = "Pidilite"
description =  "I made some double spread illustrations for a booklet called Atoot Rishtey for Fevicol adhesive brand of Pidilite Industries"
keywords = ["Pidilite", "atoot rishtey", "atoot", "fevicol", "illustrations" , "booklet illustrations", "Mumbai", "graphic design"]
slug = "pidilite-fevicol-atoot-rishtey"
+++

I made some double spread illustrations for a booklet called Atoot
Rishtey for Fevicol adhesive brand of [Pidilite
Industries](http://www.pidilite.com/) . The illustrations depict a
couple enjoying holidays in various locations such as Switzerland, Goa,
Kerala, Dubai, etc.

## Kerala

![Pidilite Kerala Fevicol](/img/pidilite/kerala.jpg)

## Goa

![Pidilite goa Fevicol](/img/pidilite/goa.jpg)

## Abu Dhabi & Dubai

![Pidilite abudhabi Fevicol](/img/pidilite/abudhabi.jpg)

## Lavasa, Mt. Abu and Adlabs Imagica

![Pidilite Lavasa, Mt. Abu and Adlabs Imagica Fevicol](/img/pidilite/mt-abu.jpg)

Inner cover depicting the husband showing his wife the gift voucher
from pidilite

![Pidilite inner cover Fevicol](/img/pidilite/inside.jpg)

## cover page

![Pidilite cover Fevicol](/img/pidilite/cover.jpg)

## Switzerland

![Pidilite cover Fevicol](/img/pidilite/swiss.jpg)

A gif showing the process

![Pidilite process Switzerland](/img/pidilite/one.gif)
