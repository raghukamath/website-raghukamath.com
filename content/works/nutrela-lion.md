+++
title = "Mascot for Nutrela"
date = "2017-02-27T15:27:00+05:30"
workthumb = "/img/nutrela-lion-thumb.jpg"
thumb = "/img/nutrela-lion-thumb-600.jpg"
tags = ["realistic", "mascot"]
client = "Design Orb, Ruchi Soya"
description = "A Mascot design and illustration done for Nutrela plus soya chunks"
keywords = ["ruchi soya", "illustration", "India", "lion illustration", "lion mascot", "nutrela mascot", "nutrela", "soya chunks", "soya for kids"]
slug = "mascot-for-nutrela"
+++

I did a mascot illustration for Design Orb and [Ruchi
soya](https://en.wikipedia.org/wiki/Ruchi_Soya). Ruchi Soya is the
largest manufacturer of edible oil in India. They also produce Soya
foods. This mascot will be used in package of Nutrela plus soya chunks,
designed by Design orb.

![Nutrela soya chunks mascot](/img/nutrela-mascot/nutrela-lion.jpg)

![Nutrela soya chunks mascot](/img/nutrela-mascot/nutrela-lion-detail.jpg)

I also created a coloring book for them, these are the cover pages for the book.

![Nutrela soya chunks mascot](/img/nutrela-mascot/nutrella-cover-01.jpg)

![Nutrela soya chunks mascot](/img/nutrela-mascot/nutrella-cover-02.jpg)

Artwork and Mascot design are copyright of Ruchi Soya Industries.
