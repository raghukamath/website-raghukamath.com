---
title: Contact
type: page
layout: static
---

## Commissions

For commission requests and enquiries please mail me with complete details of the project such as brief description of the project, deadline and schedule etcetra at the following email address:
[raghu@raghukamath.com](mailto:raghu@raghukamath.com?Subject=enquiry)

## Contacting on other platforms

- IRC - raghukamath (#freenode)
- Matrix - [@raghukamath](https://matrix.to/#/@raghukamath:matrix.org) (Alternative to whatsapp and telegram)
- Mastodon - [raghukamath@mastodon.art](https://mastodon.art/@raghukamath) (Free alternative to Twitter)
- Invent - [kamathraghavendra](https://invent.kde.org/kamathraghavendra) (KDE community code repository)
- Dribble - [raghukamath](https://dribbble.com/raghukamath)
- My Studio - [emblik.studio](https://emblik.studio)
