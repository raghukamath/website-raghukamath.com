+++
title = "Festival of lights - timelapse painting"
date = "2016-12-24T16:07:00+05:30"
tags = ["process"]
thumb = "/img/process-fol-thumb.jpg"
description = "I got an opportunity to be included in the first Krita Art book. This is a time lapse video of the painting."
keywords = ["diwali", "lights", "illustration", "process", "tutorial", "speed painting", "krita", "linux artists", "krita artbook"]
slug ="festival-of-lights-time-lapse-painting"
+++

I am grateful to krita foundation for giving me an opportunity of getting published among the fine artists from all over the world. My illustration is chosen for the first art book by Krita foundation called **Made with Krita**. This art book will be given as rewards for the backer of the kick-starter [campaign](https://www.kickstarter.com/projects/krita/krita-2016-lets-make-text-and-vector-art-awesome)  

I recorded the process of the painting, below is the time lapse video of the process.

{{< video src="/videos/Festival-of-lights-Made-in-Krita.mp4" poster="festival-of-light.jpg"  type="mp4" >}}

The completed illustration can be found [here](http://raghukamath.com/works/festival-of-lights).
