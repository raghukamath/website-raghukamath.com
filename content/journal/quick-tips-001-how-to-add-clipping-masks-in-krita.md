+++

title = "Quick Tips 001 How To Add Clipping Masks In Krita"
date = "2020-06-08T18:48:00+05:30"
tags = ["Krita", "CC-BY", "open source", "tutorials" ]
thumb = "/img/videos-thumbs/qt001-thumb-01.jpg"
description = "This is the first episode in the quick tip series. Let us check how to add clipping masks in Krita."
keywords = ["krita", "Basics", "Tutorial", "digital art with foss", "interface basics", "UI", "krita" ]
slug = "quick-tips-001"

+++

This is the first episode in the quick tip series. Let us check how to add clipping masks in Krita.

{{< video src="/videos/001-inherit-alpha.mp4" poster="qt001-thumb-ep01.jpg"  type="mp4" >}}

[Download the video](/videos/001-inherit-alpha.mp4)
