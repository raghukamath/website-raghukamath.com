+++
title = "Experiments with linocut"
date =  "2016-01-07T23:21:00+05:30"
tags = ["process"]
thumb = "/img/linocut-thumb.jpg"
description = "Some experiments with linocut Prints, It was really fun and exhilarating process. I collaborated with one my friend. The prints turned out well"
keywords = ["linocut", "Mumbai", "illustration", "linocut in india", "sameer kulkarni"]
slug="experiments-with-linocut"
+++

Last year in early March me and my friend
[Sameer](http://coolkarnisam.blogspot.in/) decided to try out Linocut. I
thought documenting the process will be good.

I am fairly late in documenting this. We collaborated in producing some
canvas murals for a client , the canvases were done in acrylic paints.
We mostly work in digital medium so after having a taste of traditional
medium we thought trying out some printing technique would be great fun.
It turned out a really awesome experience.

![linocut prints in Mumbai](/img/linocut/linocut-01.jpg)

![linocut prints in Mumbai](/img/linocut/linocut-02.jpg)

We first thought of trying out woodblock prints, but we later settled on
linoleum blocks. We got some linoleum blocks and the cutters and inks.
Finding the right ink was an adventure in itself. We got acquainted the
ink supplier, he was a good person. He shared his concerns about how
digital medium has reduced the people doing traditional printing. He was
glad that we are trying it.

The process itself was very fun, chipping out the negative space in the
linoleum was very different from the monotonous work we do otherwise.
The main challenge was that there was no "undo" or Ctrl + Z not even a
eraser would help, so planning was crucial, once you carve a design in
the linoleum sheet you can't revert it :)

![linocut prints in Mumbai](/img/linocut/linocut-03.jpg)

![linocut prints in Mumbai](/img/linocut/linocut-04.jpg)

![linocut in Mumbai](/img/linocut/linocut-05.jpg)

![linocut prints in Mumbai](/img/linocut/linocut-06.jpg)

I hope to do more prints this year, It will also be challenging to try
some of the harder and intricate designs. As we were trying this out for
the first time there were some mistakes and happy accidents. It was a
good learning exercise, some times doing something out of your comfort
zone is fun in its own way.

![linocut prints in Mumbai](/img/linocut/linocut-07.jpg)
