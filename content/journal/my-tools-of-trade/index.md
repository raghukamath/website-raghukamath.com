+++
title = "My Tools Of Trade"
date = 2020-07-16T18:00:44+05:30
thumb = "/img/tot-thumb-600.jpg"
tags = ["process", "open source"]
keywords = ["Free Software", "tools", "hardware", "krita", "Scribus", "Inkscape", "GIMP"]
description = "The variety of tools that I use for my work and day to day tasks"
draft = false
+++

I have [written]( {{<relref "/journal/Switch-to-Freedom" >}} ) earlier about how I made the switch to free software workflow and the experience I had after making the switch. It has been 6 years since then and I haven’t yet regretted my decision or found any trouble doing my work. Here is a list of software, hardware, and other tools that I use for day to day [work]( {{<relref "/works" >}}), entertainment, and computing needs. I will try to keep this page updated as I include more tools or drop some of them in the future. Apart from this list, I will write another post explaining in more detail the software that I use.


## On My Desk 🖥

![My Desk](EUDXl3DUwAATK54.jpg)

- [Wacom Intuos Pro Medium](https://www.wacom.com/en-us/products/pen-tablets/wacom-intuos-pro)
- Self Assembled computer with:
  - Intel® Core™ i7-4790K 
  - 32 GiB of memory
  - GeForce GTX 750 Ti
- [BenQ SW2700PT](https://www.benq.com/en-in/monitor/photographer/sw2700pt.html) (primary)
- Dell S2409W 24" (secondary)
- [Logitech Z625 speakers](https://www.logitech.com/en-in/product/powerful-thx-sound-z625)
- [TVS Gold Mechanical keyboard](https://www.tvs-e.in/gold-bharat/)
- Canon Lide 220 Scanner
- Brother HL-1201
- [i1 Display Pro color calibration](https://www.xrite.com/categories/calibration-profiling/i1display-pro)
- Dell Inspiron 15
- Moto g4 - Lineage OS without google apps and service.

  
## Software (Both Desktop & Laptop)

![My desktop](screenshot.png)

- [Arch Linux](https://www.archlinux.org/) - Bleeding edge no non-sense Linux (zen kernel) based operating system.
- [KDE Plasma 5](https://kde.org/plasma-desktop) - Simple yet powerful desktop environment with feature rich apps such as Konsole Kate etc.
- [Krita](https://krita.org) - A very robust professional digital painting application.
- [Mypaint](http://mypaint.org/) - A lightweight sketching and painting application
- [Gnu Image Manipulation Program](https://www.gimp.org/) - An excellent software for image retouching and adjustment.
- [Inkscape](https://inkscape.org/) - An excellent vector illustration suite.
- [Blender](https://blender.org) - Swiss army knife for anything between 3d modelling to video editing.
- [Scribus](https://www.scribus.net/) - Desktop publishing
- [Borg](https://www.borgbackup.org/) + [btrbk](https://digint.ch/btrbk/) - For incremental daily and hourly backups on internal as well as external disks.
- [Nextcloud](https://nextcloud.com/) - File, contacts, todo list & calendar sync. 
- Kmail - KDE's mail application.
- [Element](https://element.io/) & [Quassel](https://quassel-irc.org/) - for IRC and chat with friends and family

## Self-Hosted software 🌐
- [Bitwarden](https://github.com/dani-garcia/bitwarden_rs) - Password manager.
- [Mailinabox](https://mailinabox.email/) - Email and cloud hosting solution.
- [Wallabag](https://www.wallabag.it/en/) - Save or archive articles for later reading.
- Quassel - IRC client.
- [Fresh RSS](https://freshrss.org/) - RSS feed aggregator.
