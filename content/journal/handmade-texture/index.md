+++
title = "Seamless Handmade Textures"
date = "2019-11-30T12:17:00+05:30"
tags = ["open source", "textures", "resources", "CC-BY-SA"]
thumb = "/journal/handmade-texture/handmade-texture-thumb-600.jpg"
description = "Sharing some of the handmade texture that I created with Krita while working on various projects"
keywords = "Krita, handmade texture, texture pack, krita, linux, cc-by, free"
+++


![Seamless handmade texture](/journal/handmade-texture/handmade-texture-hero.jpg)

I am sharing these free seamless handmade or hand-painted textures.These were made while I was working on various projects. Not all are hand-painted some are scanned and prepared from the original item, for example, the cartridge texture is a scan of a paper, some might have made by using resources in the public domain. These are shared under [CC-BY-SA](https://creativecommons.org/licenses/by-sa/4.0/) license, so you are free to reuse modify or share these. I have added all these to a Gitlab [repository](https://gitlab.com/raghukamath/Handmade-textures) and I'll be updating and adding more to this repository as and when I make a new texture. I also welcome any contribution to this repository. You can download the entire repository along with the textures and their source files from [here](https://gitlab.com/raghukamath/Handmade-textures/-/archive/master/Handmade-textures-master.zip). If you want to download individual textures click on the thumbnails below.


<div class="grid-of-three">

<div>
<p class="raw-para"><img src="/journal/handmade-texture/001-cement-texture.jpg" alt="free seamless cement texture"><a target="_blank" href="https://gitlab.com/raghukamath/Handmade-textures/raw/master/kra/output/hires/001-cement-texture-hires.png">
Cement plaster texture</a></p>
</div>

<div>
<p class="raw-para"><img src="/journal/handmade-texture/002-brick-seamless.jpg" alt="free seamless brick texture"><a target="_blank" href="https://gitlab.com/raghukamath/Handmade-textures/raw/master/kra/output/hires/002-brick-seamless-hires.png">Seamless brick texture</a>
</p>
</div>

<div>
<p class="raw-para"><img src="/journal/handmade-texture/003-leaves-seamless.jpg" alt="free seamless fallen leaves texture"><a target="_blank" href="https://gitlab.com/raghukamath/Handmade-textures/raw/master/kra/output/hires/003-leaves-seamless-hires.png">A heap of green leaves</a>
</p>
</div>

<div>
<p class="raw-para"><img src="/journal/handmade-texture/004-cartridge-texture.jpg" alt="free seamless cartridge paper texture"><a target="_blank" href="https://gitlab.com/raghukamath/Handmade-textures/raw/master/kra/output/hires/004-cartridge-texture-hires.png">Seamless cartridge paper texture</a>
</p>
</div>

<div>
<p class="raw-para"><img src="/journal/handmade-texture/005-mdf-seamless.jpg" alt="free seamless mdf panel texture"><a  target="_blank" href="https://gitlab.com/raghukamath/Handmade-textures/raw/master/kra/output/hires/005-mdf-seamless-hires.png">Seamless MDF board texture</a>
</p>
</div>

<div>
<p class="raw-para"><img src="/journal/handmade-texture/006-wall-plaster.jpg" alt="free seamless wall plaster texture"><a  target="_blank" href="https://gitlab.com/raghukamath/Handmade-textures/raw/master/kra/output/hires/006-wall-plaster-hires.png">Seamless wall plaster texture</a>
</p>
</div>

<div>
<p class="raw-para"><img src="/journal/handmade-texture/007-mosaic-tiles-lowres.jpg" alt="free seamless cement mosaic texture"><a  target="_blank" href="https://gitlab.com/raghukamath/Handmade-textures/-/raw/master/kra/output/hires/007-mosaic-tiles-hires.png">Seamless cement mosaic texture</a>
</p>
</div>

</div>

If you find these useful share them with your fellow artists and spread the word, I will also be excited to see how other artists have used these in their artwork. These are free to download but if you like to support me by donating money, you can use my [PayPal link](https://www.paypal.me/raghukamath).
