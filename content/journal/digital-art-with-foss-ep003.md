+++
title = "Digital art with foss episode 3"
date = "2018-04-29T21:48:00+05:30"
tags = ["Krita", "CC-BY", "open source", "tutorials" ]
thumb = "/img/videos-thumbs/daef-thumb-01.jpg"
description = "In this episode we will learn how to navigate around the canvas in Krita using shortcuts and gui tools"
keywords = ["krita", "Basics", "Tutorial", "digital art with foss", "interface basics", "UI", "krita"]
slug = "digital-art-with-foss-episode-03"
+++

In this episode we will learn how to navigate around the canvas in
Krita. This video will have lots of shortcut so for your convenience I
have prepared a PDF with list of these shortcuts as a PDF with all the
shortcuts mentioned in this video, you can download it from the link
[here](https://raghukamath.com/course/ep03/Krita-navigation-shortcuts.pdf).

{{< video src="/videos/Digital-art-with-krita-ep03-navigation.mp4" poster="video-thumb-ep03.jpg" type="mp4" >}}

[Download the video](/videos/Digital-art-with-krita-ep03-navigation.mp4)

## Episode Credits

-   Sitar music in the background by
    [Kaiho](https://freesound.org/people/Kaiho) Licensed under Creative
    Commons Attribution 3.0 Unported (CC BY 3.0)
