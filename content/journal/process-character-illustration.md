+++
title = "Process - character illustration"
date = "2015-11-30T23:18:00+05:30"
tags = ["process"]
thumb = "/img/process-maharaja.jpg"
description = "Recorded my process for the first time. It was a great experience to analyse my process while viewing as a third person. Can’t say this is a tutorial, but I tried to explain certain things on the go."
keywords = ["tutorial", "floss digital painting", "linux artists", "process", "speed-painting"]
slug = "process-character-illustration"

+++

Recorded my process for the first time. It was a great experience to
analyse my process while viewing as a third person. Can't say this is a
tutorial, but I tried to explain certain things on the go. I'll improve
next time.

## Part 1

{{< video src="/videos/Illustrating_a_Character_in_Krita.mp4" poster="maharaja-part1.jpg"  type="mp4" >}}

## Part 2

{{< video src="/videos/Illustrating_a_Character_in_Krita_Part_2.mp4" poster="maharaja-part2.jpg"  type="mp4" >}}

P.S. I am in love with this application - [Krita](https://krita.org/).
Let me know what you think.
