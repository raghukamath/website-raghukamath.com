---
title: Reading
type: page
layout: static
---

# Reading List

I will try to maintain a list of books and e-books that I am currently reading and plan to read in future.


## Currently  Reading 📖


|Title   | Author             | Started on    |
|:------:|:------------------:|:--------------:|
|[रश्मिरथी](https://en.wikipedia.org/wiki/Rashmirathi)   | श्री रामधारी सिंह "दिनकर" | 21 July 2020  |



