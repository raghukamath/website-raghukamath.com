FROM debian:9 as builder

COPY . /var/www/blog

# Install packages
RUN apt-get -qq update \
    && DEBIAN_FRONTEND=noninteractive apt-get -qq install -y --no-install-recommends \
    python-pygments \
    git \
    ca-certificates \
    asciidoc \
    curl \
    && rm -rf /var/lib/apt/lists/*

# Download and install hugo
ENV HUGO_VERSION 0.60.1

ENV HUGO_BINARY hugo_${HUGO_VERSION}_Linux-64bit.deb

RUN curl -sL -o /tmp/hugo.deb \
    https://github.com/spf13/hugo/releases/download/v${HUGO_VERSION}/${HUGO_BINARY} \
    && dpkg -i /tmp/hugo.deb \
    && rm /tmp/hugo.deb \
    && mkdir -p /var/www/blog

WORKDIR /var/www/blog
RUN hugo

# Distributable image layer
FROM nginx:alpine
COPY --from=builder /var/www/blog/public/ /usr/share/nginx/html

